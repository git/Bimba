Version 3.1 bietet:
* monochromes Launcher-Symbol und vorausschauende Zurück-Geste für Android 12,
* Material You-Thema mit Systemfarben,
* neue Suchleiste,
* Suche nach Open Location Code,
* Gleiten statt Springen in der Minimap,
* Polnische und italienische Übersetzungen,
* Handhabung mehrerer Feeds,
* QR-Codes in Metropolis GZM,
* Suche nach Linien (und Auswahl von Haltestellen in Graphen),
* Aktualisierungen von Abhängigkeiten, Fehlerbehebungen und einige Code-Überarbeitungen.
