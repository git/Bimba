Changes in version 3.3
* Added selecting date and filtering by time
* Added filtering departures by line
* Added alerts shown in stop and departures
* Fixed landscape version of ‘about’ screen
* Fixed capabilities for vehicles on map
* Changed cached localities to lower contrast
* Fixed showing line directions for other than 2
* Fixed saving localities cache
* Updated dependencies
* Updated deprecated methods in code
