Bimba lets you check public transport timetable with realtime departures in multiple cities.

Current features:
* checking departures by stop,
* searching nearby stops,
* searching stops by QR code (in selected timetables),
* searching stops on line,
* vehicle attributes for departures (depending on available information),
* vehicles and stops on map in real time,
