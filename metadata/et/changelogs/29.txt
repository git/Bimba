Muudatused versioonis 3.5:
* lisasime otsinguvõimaluse plusskoodide alusel
* lisasime nooled, mis viitavad lähimale peatusele
* lisasime erinevate ühikute kasutamise võimaluse (kiirus, vahemaa, aeg)
