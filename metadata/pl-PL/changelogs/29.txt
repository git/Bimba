Zmiany w wersji 3.5:
* dodano szukanie krótkimi kodami OLC
* dodano strzałki pokazujące kierunek do najbliższych przystanków
* dodano jednostki (szybkości, odległości, czasu)
