Zmiany w wersji 3.6:
* Opcje przesiadki mogą pokazywać linie kursywą
* Błędy mogą być raportowane
* Strona o Bimbie zawiera więcej możliwości kontaktu
* Naprawiono aktualizowanie ulubionych i danych geokodowania na starszych wersjach Androida
