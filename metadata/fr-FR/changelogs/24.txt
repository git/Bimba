Changements dans la version 3.2 :
* Ajout d'un écran "A propos"
* le défilement vers le bas charge plus de départs
* les informations de localités et les paramètres sont mis en cache hors ligne
* les listes mettent seulement à jour les éléments modifiés
* les résultats de recherche incluent le nom de la localité
* La liste des localités peut être filtrée (plus de 12 éléments)
* changement des icônes d'arrêts
* les icônes sur la carte sont mises à l'échelle
* TRAFFIC version clockworkOrange compatibility
* Last update of departures is shown if over a minute
* New German translations
* Bimba est conforme REUSE
