Changes in version 3.5:
* ajout d’une recherche via Plus Codes courts
* ajouts de flèches indiquant l’arrêt le plus proche
* ajout des unités (vitesse, distance, temps)
