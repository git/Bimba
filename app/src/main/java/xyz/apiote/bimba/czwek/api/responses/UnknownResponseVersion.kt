// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api.responses

class UnknownResponseVersion(resource: String, version: ULong) :
	Exception("Unknown resource $resource in version $version")

