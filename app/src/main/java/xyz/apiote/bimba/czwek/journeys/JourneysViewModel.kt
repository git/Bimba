// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later
package xyz.apiote.bimba.czwek.journeys

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import xyz.apiote.bimba.czwek.repo.Journey
import xyz.apiote.bimba.czwek.repo.JourneyParams
import xyz.apiote.bimba.czwek.repo.Place
import java.net.SocketTimeoutException

class JourneysViewModel : ViewModel() {

	private val _journeys = MutableLiveData<List<Journey>>()
	val journeys: LiveData<List<Journey>> = _journeys

	// FIXME when not in foreground, throws java.net.SocketException: Software caused connection abort
	fun getJourneys(context: Context, origin: Place, destination: Place, params: JourneyParams) {
		viewModelScope.launch {
			try {
				_journeys.value =
					xyz.apiote.bimba.czwek.api.getJourney(origin, destination, params, context)
			} catch (e: SocketTimeoutException) {
				_journeys.value = emptyList<Journey>()
				Log.e("Journeys", "timeout: $e")
			}
		}
	}
}
