// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.textfield.TextInputEditText
import xyz.apiote.bimba.czwek.repo.Place
import xyz.apiote.bimba.czwek.repo.TimeReference
import xyz.apiote.bimba.czwek.search.Query
import java.time.LocalDate
import java.time.LocalTime

class DashboardViewModel : ViewModel() {
	companion object {
		const val ORIGIN_KEY = "ORIGIN"
		const val DEST_KEY = "DESTINATION"

		val keys = arrayOf(ORIGIN_KEY, DEST_KEY)
		val indices = mapOf(ORIGIN_KEY to 0, DEST_KEY to 1)

		fun otherSource(source: String): String = keys[(indices[source]!! + 1) % 2]
	}

	val mutableData = mapOf(
		ORIGIN_KEY to MutableLiveData<Place>(),
		DEST_KEY to MutableLiveData<Place>()
	)

	val data = mapOf<String, LiveData<Place>>(
		ORIGIN_KEY to mutableData[ORIGIN_KEY]!!,
		DEST_KEY to mutableData[DEST_KEY]!!
	)

	fun set(source: String, place: Place) {
		mutableData[source]!!.value = place
	}

	fun unset(source: String) {
		mutableData[source]!!.value = null
	}

	val spans = mutableMapOf(
		ORIGIN_KEY to "",
		DEST_KEY to ""
	)

	val positionQueries = mutableMapOf<String, Query?>(
		ORIGIN_KEY to null,
		DEST_KEY to null
	)

	val suggestions = mutableMapOf<String, ChipGroup>(
	)

	val textInputs = mutableMapOf<String, TextInputEditText>()

	var timeReference: TimeReference = TimeReference.DEPART_AFTER
	var date: Long? = null
	var time: LocalTime? = null
	var wheelchairAccessible: Boolean = false
	var bicycle: Boolean = false

}
