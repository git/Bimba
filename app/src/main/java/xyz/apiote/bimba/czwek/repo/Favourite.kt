// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

data class Favourite(
	val sequence: Int?,
	val feedID: String,
	val feedName: String,
	val stopCode: String,
	val stopName: String,
	val lines: List<String>,
	val exact: Boolean
)