// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek

import android.content.res.Resources
import android.util.DisplayMetrics
import kotlin.math.roundToInt

fun dpToPixel(dp: Float): Float {
	val metrics: DisplayMetrics = Resources.getSystem().displayMetrics
	return dp * (metrics.densityDpi / 160f)
}

fun dpToPixelI(dp: Float): Int = dpToPixel(dp).roundToInt()