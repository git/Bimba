// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api.structs

import xyz.apiote.bimba.czwek.api.UnknownResourceVersionException

enum class QrLocationV1 {
	UNKNOWN, NONE, PATH, QUERY;

	companion object {
		fun of(q: UInt): QrLocationV1 {
			return when (q) {
				0u -> UNKNOWN
				1u -> NONE
				2u -> PATH
				3u -> QUERY
				else -> throw UnknownResourceVersionException("QrLocation/$q", 1u)
			}
		}
	}
}