// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek

import android.content.Context
import androidx.core.app.NotificationManagerCompat
import org.acra.BuildConfig
import org.acra.config.httpSender
import org.acra.config.notification
import org.acra.data.StringFormat
import org.acra.ktx.initAcra
import org.acra.security.TLS
import org.acra.sender.HttpSender
import org.osmdroid.config.Configuration
import java.io.File

class Bimba : android.app.Application() {
	override fun onCreate() {
		super.onCreate()
		Configuration.getInstance()
			.let { config ->
				config.load(
					applicationContext,
					applicationContext.getSharedPreferences("shp", MODE_PRIVATE)
				)
				config.osmdroidBasePath = File(applicationContext.cacheDir.absolutePath, "osmdroid")

				config.osmdroidTileCache = File(config.osmdroidBasePath.absolutePath, "tile")
			}
	}

	override fun attachBaseContext(base: Context) {
		super.attachBaseContext(base)

		initAcra {
			buildConfigClass = BuildConfig::class.java
			reportFormat = StringFormat.JSON

			httpSender {
				uri = "https://bimba.apiote.xyz/acra/send"
				httpMethod = HttpSender.Method.POST
				tlsProtocols = listOf(TLS.V1_3, TLS.V1_2)
			}

			notification {
				title = getString(R.string.acra_notification_title)
				text = getString(R.string.acra_notification_text)
				channelName = getString(R.string.acra_notification_channel)
				channelDescription = getString(R.string.acra_notification_channel_description)
				channelImportance = NotificationManagerCompat.IMPORTANCE_DEFAULT
				sendButtonText = getString(R.string.send)
				resSendButtonIcon = R.drawable.send
				discardButtonText = getString(R.string.discard)
				resDiscardButtonIcon = R.drawable.discard
				sendWithCommentButtonText = getString(R.string.send_with_comment)
				resSendWithCommentButtonIcon = R.drawable.comment
				commentPrompt = getString(R.string.acra_notification_comment)
			}
		}
	}
}