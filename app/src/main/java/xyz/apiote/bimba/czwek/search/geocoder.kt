// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.search

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.location.Location

fun findPlace(context: Context, name: String): Location? {
	val db = SQLiteDatabase.openOrCreateDatabase(context.getDatabasePath("geocoding").path, null)
	val cursor = db.rawQuery(
		"select lat, lon from place_names join places using(id) where name = ?",
		arrayOf(name)
	)

	if (!cursor.moveToNext()) {
		cursor.close()
		db.close()
		return null
	}

	val location = Location(null).apply {
		latitude = cursor.getDouble(0)
		longitude = cursor.getDouble(1)
	}
	cursor.close()
	db.close()
	return location
}

class GeocodingException : Exception()