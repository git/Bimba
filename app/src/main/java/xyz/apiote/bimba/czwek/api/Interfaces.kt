// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api

interface QueryableV1
interface QueryableV2
interface QueryableV3
interface QueryableV4
interface QueryableV5
interface LocatableV1
interface LocatableV2
interface LocatableV3
interface LocatableV4