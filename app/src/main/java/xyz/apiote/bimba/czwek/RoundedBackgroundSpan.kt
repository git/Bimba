// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.text.style.ReplacementSpan

class RoundedBackgroundSpan(private val bgColour: Int, private val fgColour: Int) : ReplacementSpan() {
	override fun getSize(
		paint: Paint,
		text: CharSequence,
		start: Int,
		end: Int,
		fm: Paint.FontMetricsInt?
	): Int {
		return (paint.measureText(text, start, end)+20).toInt()
	}

	override fun draw(
		canvas: Canvas,
		text: CharSequence,
		start: Int,
		end: Int,
		x: Float,
		top: Int,
		y: Int,
		bottom: Int,
		paint: Paint
	) {
		val length = paint.measureText(text, start, end) + 20
		val rect = RectF(x, top.toFloat() - 5f, x + length, y.toFloat() + 5f)
		paint.color = bgColour
		canvas.drawRoundRect(rect, 10f, 10f, paint)
		paint.color = fgColour
		paint.textAlign = Paint.Align.CENTER
		canvas.drawText(text, start, end, x+(length/2), y.toFloat(), paint)
	}
}