// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import xyz.apiote.bimba.czwek.api.Error
import xyz.apiote.bimba.czwek.api.mapHttpError

class TrafficResponseException(code: Int, msg: String, val error: Error) :
	Exception("Error response with code $code: $msg") {

	constructor(code: Int) : this(
		code,
		"",
		Error(code, mapHttpError(code).first, mapHttpError(code).second)
	)
}