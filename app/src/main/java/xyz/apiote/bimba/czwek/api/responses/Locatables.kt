// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api.responses

import xyz.apiote.bimba.czwek.api.LocatableV1
import xyz.apiote.bimba.czwek.api.LocatableV2
import xyz.apiote.bimba.czwek.api.LocatableV3
import xyz.apiote.bimba.czwek.api.StopV1
import xyz.apiote.bimba.czwek.api.StopV2
import xyz.apiote.bimba.czwek.api.UnknownResourceVersionException
import xyz.apiote.bimba.czwek.api.VehicleV1
import xyz.apiote.bimba.czwek.api.VehicleV2
import xyz.apiote.bimba.czwek.api.VehicleV3
import xyz.apiote.fruchtfleisch.Reader
import java.io.InputStream

interface LocatablesResponse {
	companion object {
		fun unmarshal(stream: InputStream): LocatablesResponse {
			val reader = Reader(stream)
			return when (val v = reader.readUInt().toULong()) {
				// 0UL -> LocatablesResponseDev.unmarshal(stream)
				1UL -> LocatablesResponseV1.unmarshal(stream)
				2UL -> LocatablesResponseV2.unmarshal(stream)
				3UL -> LocatablesResponseV3.unmarshal(stream)
				else -> throw UnknownResponseVersion("Locatables", v)
			}
		}
	}
}


data class LocatablesResponseV3(val locatables: List<LocatableV3>) : LocatablesResponse {
	companion object {
		fun unmarshal(stream: InputStream): LocatablesResponseV3 {
			val locatables = mutableListOf<LocatableV3>()
			val reader = Reader(stream)
			val n = reader.readUInt().toULong()
			for (i in 0UL until n) {
				when (val r = reader.readUInt().toULong()) {
					0UL -> {
						locatables.add(StopV2.unmarshal(stream))
					}

					1UL -> {
						locatables.add(VehicleV3.unmarshal(stream))
					}

					else -> {
						throw UnknownResourceVersionException("Locatable/$r", 0u)
					}
				}
			}
			return LocatablesResponseV3(locatables)
		}
	}
}

data class LocatablesResponseV2(val locatables: List<LocatableV2>) : LocatablesResponse {
	companion object {
		fun unmarshal(stream: InputStream): LocatablesResponseV2 {
			val locatables = mutableListOf<LocatableV2>()
			val reader = Reader(stream)
			val n = reader.readUInt().toULong()
			for (i in 0UL until n) {
				when (val r = reader.readUInt().toULong()) {
					0UL -> locatables.add(StopV2.unmarshal(stream))
					1UL -> locatables.add(VehicleV2.unmarshal(stream))
					else -> throw UnknownResourceVersionException("Locatable/$r", 2u)
				}
			}
			return LocatablesResponseV2(locatables)
		}
	}
}

data class LocatablesResponseV1(val locatables: List<LocatableV1>) : LocatablesResponse {
	companion object {
		fun unmarshal(stream: InputStream): LocatablesResponseV1 {
			val locatables = mutableListOf<LocatableV1>()
			val reader = Reader(stream)
			val n = reader.readUInt().toULong()
			for (i in 0UL until n) {
				when (val r = reader.readUInt().toULong()) {
					0UL -> {
						locatables.add(StopV1.unmarshal(stream))
					}

					1UL -> {
						locatables.add(VehicleV1.unmarshal(stream))
					}

					else -> {
						throw UnknownResourceVersionException("Locatable/$r", 1u)
					}
				}
			}
			return LocatablesResponseV1(locatables)
		}
	}
}
