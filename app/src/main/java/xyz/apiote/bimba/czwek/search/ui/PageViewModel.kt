// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.search.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.bandb.graphview.graph.Graph
import dev.bandb.graphview.graph.Node
import xyz.apiote.bimba.czwek.repo.LineGraph

class PageViewModel : ViewModel() {

	private val _data = MutableLiveData<Graph>()
	val data: LiveData<Graph> = _data

	fun setupGraphView(lineGraph: LineGraph) {
		val graph = Graph()
		val nodes = lineGraph.stops.map { Node(it) }
		lineGraph.nextNodes.filter { it.key != -1L }.forEach { (from, tos) ->
			tos.filter { it != -1L }.forEach { to ->
				graph.addEdge(nodes[from.toInt()], nodes[to.toInt()])
			}
		}
		_data.value = graph
	}
}