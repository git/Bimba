// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.dashboard.ui.home

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.openapitools.client.infrastructure.ServerException
import xyz.apiote.bimba.czwek.repo.Event
import xyz.apiote.bimba.czwek.repo.Favourite
import xyz.apiote.bimba.czwek.repo.FeedInfo
import xyz.apiote.bimba.czwek.repo.OfflineRepository
import xyz.apiote.bimba.czwek.repo.OnlineRepository
import xyz.apiote.bimba.czwek.repo.Queryable
import xyz.apiote.bimba.czwek.repo.TrafficResponseException
import xyz.apiote.bimba.czwek.settings.feeds.FeedsSettings
import java.sql.SQLException
import java.util.Optional

class HomeViewModel : ViewModel() {
	private val mutableQueryables = MutableLiveData<List<Queryable>>()
	val queryables: LiveData<List<Queryable>> = mutableQueryables
	var feeds: Map<String, FeedInfo>? = null
	var feedsSettings: FeedsSettings? = null
	private val mutableFavourites = MutableLiveData<List<Favourite>>()
	val favourites: LiveData<List<Favourite>> = mutableFavourites
	private val mutableDepartures = MutableLiveData<Map<String, Optional<Event>>>()
	val departures: LiveData<Map<String, Optional<Event>>> = mutableDepartures

	fun getQueryables(query: String, context: Context) {
		viewModelScope.launch {
			try {
				getFeeds(context)
				mutableQueryables.value = OnlineRepository().queryQueryables(query, context) ?: emptyList()
			} catch (e: TrafficResponseException) {
				// XXX intentionally no error showing in suggestions
				Log.e("Suggestion", "$e")
			} catch (e: ServerException) {
				// XXX intentionally no error showing in suggestions
				Log.e("Suggestion", "$e")
			}
		}
	}

	fun getFavourites(context: Context) {
		viewModelScope.launch {
			try {
				getFeeds(context)
				val repository = OfflineRepository(context)
				mutableFavourites.value =
					repository.getFavourites(feedsSettings?.activeFeeds() ?: emptySet())
				repository.close()
			} catch (e: SQLException) {
				Log.w("FavouritesForFavourite", "$e")
			}
			getDeparturesOnly(context)
		}
	}

	fun getDepartures(context: Context) {
		viewModelScope.launch {
			getDeparturesOnly(context)
		}
	}

	private suspend fun getDeparturesOnly(context: Context) {
		coroutineScope {
			if (favourites.value == null)
				return@coroutineScope
			mutableDepartures.value = favourites.value!!.map { favourite ->
				async {
					try {
						val repository = OnlineRepository()
						val stopDepartures =
							repository.getDepartures(
								favourite.feedID,
								favourite.stopCode,
								null,
								context,
								12,  // XXX heuristics
								favourite.exact
							)
						stopDepartures?.let { sDs ->
							if (sDs.events.isEmpty()) {
								Pair(favourite.feedID + favourite.stopCode, Optional.empty())
							} else {
								Pair(
									favourite.feedID + favourite.stopCode,
									Optional.ofNullable(sDs.events.find { departure ->
										favourite.lines.isEmpty() or favourite.lines.contains(
											departure.vehicle.Line.name
										)
									})
								)
							}
						} ?: Pair(favourite.feedID + favourite.stopCode, Optional.empty())
					} catch (e: TrafficResponseException) {
						Log.w("DeparturesForFavourite", "$e")
						Pair(favourite.feedID + favourite.stopCode, Optional.empty())
					} catch (e: ServerException) {
						Log.w("DeparturesForFavourite", "Transitous returned ${e.statusCode}, ${e.message}")
						Pair(favourite.feedID + favourite.stopCode, Optional.empty())
					}
				}
			}.awaitAll().associate { it }
		}
	}

	private suspend fun getFeeds(context: Context) {
		val repository = OfflineRepository(context)
		feeds = repository.getFeeds(context)
		repository.close()
		feedsSettings = FeedsSettings.load(context)
	}

	fun saveFavourites(newFavourites: List<Favourite>, context: Context) {
		viewModelScope.launch {
			try {
				val repository = OfflineRepository(context)
				repository.saveFavourites(newFavourites.toSet())
				mutableFavourites.value = newFavourites
				repository.close()
			} catch (e: SQLException) {
				Log.w("FavouritesForFavourite", "$e")
			}
		}
	}


	inner class SearchBarWatcher(private val context: Context) : TextWatcher {
		private val handler = Handler(Looper.getMainLooper())
		private var workRunnable = Runnable {}

		override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
		}

		override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
		}

		override fun afterTextChanged(s: Editable?) {
			handler.removeCallbacks(workRunnable)
			workRunnable = Runnable {
				val text = s.toString()
				getQueryables(text, context)
			}
			handler.postDelayed(workRunnable, 750)
		}
	}
}
