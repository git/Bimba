// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.dashboard.ui.journey

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModel
import com.google.android.material.chip.Chip

class JourneyViewModel : ViewModel() {

	var hereChipRequester: String? = null
	var searchRequester: String? = null

	@SuppressLint("StaticFieldLeak")
	var loadingChip: Chip? = null

}
