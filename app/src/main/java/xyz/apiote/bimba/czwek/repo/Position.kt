// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.location.Location
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import xyz.apiote.bimba.czwek.api.PositionV1

@Parcelize
data class Position(val latitude: Double, val longitude: Double): Parcelable {
	init {
		if (latitude > 90 || latitude < -90 || longitude > 180 || longitude < -180) {
			throw IllegalArgumentException("$latitude must be [-90, 90], $longitude must be [-180, 180]")
		}
	}

	constructor(p: PositionV1) : this(p.latitude, p.longitude)

	fun isZero(): Boolean {
		return latitude == 0.0 && longitude == 0.0
	}

	fun distanceTo(other: Position): Float {
		return Location(null).apply {
			latitude = this@Position.latitude
			longitude = this@Position.longitude
		}.distanceTo(Location(null).apply {
			latitude = other.latitude
			longitude = other.longitude
		})
	}

	override fun toString(): String {
		return "$latitude,$longitude"
	}

	companion object {
		fun comparator(centre: Position) = object : Comparator<Position> {
			override fun compare(
				o1: Position?,
				o2: Position?
			): Int {
				if (o1 == null || o2 == null) {
					throw NullPointerException()
				}

				return o1.distanceTo(centre).compareTo(o2.distanceTo(centre))
			}
		}
	}
}
