// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import xyz.apiote.bimba.czwek.api.LineStubV1
import xyz.apiote.bimba.czwek.api.LineStubV2
import xyz.apiote.bimba.czwek.api.LineStubV3

@Parcelize
data class LineStub(val name: String, val kind: LineType, val colour: Colour) : LineAbstract, Parcelable {
	constructor(l: LineStubV1) : this(l.name, LineType.of(l.kind), Colour(l.colour))
	constructor(l: LineStubV2) : this(l.name, LineType.of(l.kind), Colour(l.colour))
	constructor(l: LineStubV3) : this(l.name, LineType.of(l.kind), Colour(l.colour))

	fun icon(context: Context, scale: Float = 1f): Drawable {
		return BitmapDrawable(context.resources, super.icon(context, kind, colour, scale))
	}
}