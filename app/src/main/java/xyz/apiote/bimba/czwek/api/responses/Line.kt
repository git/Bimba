// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api.responses

import xyz.apiote.bimba.czwek.api.LineV1
import xyz.apiote.bimba.czwek.api.LineV2
import xyz.apiote.bimba.czwek.api.LineV3
import xyz.apiote.fruchtfleisch.Reader
import java.io.InputStream

interface LineResponse {
	companion object {
		fun unmarshal(stream: InputStream): LineResponse {
			val reader = Reader(stream)
			return when (val v = reader.readUInt().toULong()) {
				// 0UL -> LineResponseDev.unmarshal(stream)
				1UL -> LineResponseV1.unmarshal(stream)
				2UL -> LineResponseV2.unmarshal(stream)
				3UL -> LineResponseV3.unmarshal(stream)
				else -> throw UnknownResponseVersion("Line", v)
			}
		}
	}
}

data class LineResponseV3(
	val line: LineV3
) : LineResponse {
	companion object {
		fun unmarshal(stream: InputStream): LineResponseV3 {
			return LineResponseV3(LineV3.unmarshal(stream))
		}
	}
}

data class LineResponseV2(
	val line: LineV2
) : LineResponse {
	companion object {
		fun unmarshal(stream: InputStream): LineResponseV2 {
			return LineResponseV2(LineV2.unmarshal(stream))
		}
	}
}

data class LineResponseV1(
	val line: LineV1
) : LineResponse {
	companion object {
		fun unmarshal(stream: InputStream): LineResponseV1 {
			return LineResponseV1(LineV1.unmarshal(stream))
		}
	}
}