// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api.structs

import xyz.apiote.bimba.czwek.api.UnknownResourceVersionException

enum class VehicleStatusV1 {
	IN_TRANSIT, INCOMING, AT_STOP, DEPARTED;

	companion object {
		fun of(type: UInt): VehicleStatusV1 {
			return when (type) {
				0u -> IN_TRANSIT
				1u -> INCOMING
				2u -> AT_STOP
				3u -> DEPARTED
				else -> throw UnknownResourceVersionException("VehicleStatus/$type", 1u)
			}
		}
	}
}