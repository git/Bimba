// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.search.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import xyz.apiote.bimba.czwek.repo.Line

class SectionsPagerAdapter(fm: FragmentManager, val line: Line) :
	FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

	override fun getItem(position: Int): Fragment {
		return LineGraphFragment.newInstance(line.graphs[position], line.id, line.name, line.feedID)
	}

	override fun getPageTitle(position: Int): CharSequence {
		return line.headsigns[position].joinToString()
	}

	override fun getCount(): Int {
		return line.headsigns.size
	}
}
