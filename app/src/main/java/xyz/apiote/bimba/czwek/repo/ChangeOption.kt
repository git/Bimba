// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import xyz.apiote.bimba.czwek.api.ChangeOptionV1
import xyz.apiote.bimba.czwek.api.ChangeOptionV2

@Parcelize
data class ChangeOption(val line: LineStub, val headsigns: List<String>): Parcelable {
	constructor(c: ChangeOptionV1) : this(LineStub(c.line, LineType.UNKNOWN, Colour(0u,0u,0u)), listOf(c.headsign))
	constructor(c: ChangeOptionV2) : this(LineStub(c.line), c.headsigns)
}