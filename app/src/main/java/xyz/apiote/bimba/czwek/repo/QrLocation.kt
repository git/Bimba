// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import xyz.apiote.bimba.czwek.api.structs.QrLocationV1


enum class QrLocation {
	UNKNOWN, NONE, PATH, QUERY;

	companion object {
		fun of(q: QrLocationV1): QrLocation {
			return when (q) {
				QrLocationV1.UNKNOWN -> UNKNOWN
				QrLocationV1.NONE -> NONE
				QrLocationV1.PATH -> PATH
				QrLocationV1.QUERY -> QUERY
			}
		}
	}

	fun value(): UInt {
		return when (this) {
			UNKNOWN -> 0u
			NONE -> 1u
			PATH -> 2u
			QUERY -> 3u
		}
	}
}
