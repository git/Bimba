// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api.responses

import xyz.apiote.fruchtfleisch.Reader
import java.io.InputStream

data class ErrorResponse(val field: String, val message: String) {
	companion object {
		fun unmarshal(stream: InputStream): ErrorResponse {
			val reader = Reader(stream)
			return ErrorResponse(reader.readString(), reader.readString())
		}
	}
}
