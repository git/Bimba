// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api.responses

import xyz.apiote.bimba.czwek.api.AlertV1
import xyz.apiote.bimba.czwek.api.DepartureV1
import xyz.apiote.bimba.czwek.api.DepartureV2
import xyz.apiote.bimba.czwek.api.DepartureV3
import xyz.apiote.bimba.czwek.api.DepartureV4
import xyz.apiote.bimba.czwek.api.StopV1
import xyz.apiote.bimba.czwek.api.StopV2
import xyz.apiote.fruchtfleisch.Reader
import java.io.InputStream

interface DeparturesResponse {
	companion object {
		fun unmarshal(stream: InputStream): DeparturesResponse {
			val reader = Reader(stream)
			return when (val v = reader.readUInt().toULong()) {
				// 0UL -> DeparturesResponseDev.unmarshal(stream)
				1UL -> DeparturesResponseV1.unmarshal(stream)
				2UL -> DeparturesResponseV2.unmarshal(stream)
				3UL -> DeparturesResponseV3.unmarshal(stream)
				4UL -> DeparturesResponseV4.unmarshal(stream)
				else -> throw UnknownResponseVersion("Departures", v)
			}
		}
	}
}

data class DeparturesResponseV4(
	val alerts: List<AlertV1>,
	val departures: List<DepartureV4>,
	val stop: StopV2
) : DeparturesResponse {
	companion object {
		fun unmarshal(stream: InputStream): DeparturesResponseV4 {
			val alerts = mutableListOf<AlertV1>()
			val departures = mutableListOf<DepartureV4>()

			val reader = Reader(stream)
			val alertsNum = reader.readUInt().toULong()
			for (i in 0UL until alertsNum) {
				val alert = AlertV1.unmarshal(stream)
				alerts.add(alert)
			}
			val departuresNum = reader.readUInt().toULong()
			for (i in 0UL until departuresNum) {
				val departure = DepartureV4.unmarshal(stream)
				departures.add(departure)
			}

			return DeparturesResponseV4(alerts, departures, StopV2.unmarshal(stream))
		}
	}
}

data class DeparturesResponseV3(
	val alerts: List<AlertV1>,
	val departures: List<DepartureV3>,
	val stop: StopV2
) : DeparturesResponse {
	companion object {
		fun unmarshal(stream: InputStream): DeparturesResponseV3 {
			val alerts = mutableListOf<AlertV1>()
			val departures = mutableListOf<DepartureV3>()

			val reader = Reader(stream)
			val alertsNum = reader.readUInt().toULong()
			for (i in 0UL until alertsNum) {
				val alert = AlertV1.unmarshal(stream)
				alerts.add(alert)
			}
			val departuresNum = reader.readUInt().toULong()
			for (i in 0UL until departuresNum) {
				val departure = DepartureV3.unmarshal(stream)
				departures.add(departure)
			}

			return DeparturesResponseV3(alerts, departures, StopV2.unmarshal(stream))
		}
	}
}

data class DeparturesResponseV2(
	val alerts: List<AlertV1>,
	val departures: List<DepartureV2>,
	val stop: StopV2
) : DeparturesResponse {
	companion object {
		fun unmarshal(stream: InputStream): DeparturesResponseV2 {
			val alerts = mutableListOf<AlertV1>()
			val departures = mutableListOf<DepartureV2>()

			val reader = Reader(stream)
			val alertsNum = reader.readUInt().toULong()
			for (i in 0UL until alertsNum) {
				val alert = AlertV1.unmarshal(stream)
				alerts.add(alert)
			}
			val departuresNum = reader.readUInt().toULong()
			for (i in 0UL until departuresNum) {
				val departure = DepartureV2.unmarshal(stream)
				departures.add(departure)
			}

			return DeparturesResponseV2(alerts, departures, StopV2.unmarshal(stream))
		}
	}
}

data class DeparturesResponseV1(
	val alerts: List<AlertV1>,
	val departures: List<DepartureV1>,
	val stop: StopV1
) : DeparturesResponse {
	companion object {
		fun unmarshal(stream: InputStream): DeparturesResponseV1 {
			val alerts = mutableListOf<AlertV1>()
			val departures = mutableListOf<DepartureV1>()

			val reader = Reader(stream)
			val alertsNum = reader.readUInt().toULong()
			for (i in 0UL until alertsNum) {
				val alert = AlertV1.unmarshal(stream)
				alerts.add(alert)
			}
			val departuresNum = reader.readUInt().toULong()
			for (i in 0UL until departuresNum) {
				val departure = DepartureV1.unmarshal(stream)
				departures.add(departure)
			}

			return DeparturesResponseV1(alerts, departures, StopV1.unmarshal(stream))
		}
	}
}

