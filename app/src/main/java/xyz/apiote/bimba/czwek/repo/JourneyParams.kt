// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId


enum class TimeReference {
	DEPART_AFTER, ARRIVE_BY;

	fun id(): Int {
		return when (this) {
			DEPART_AFTER -> 1
			ARRIVE_BY -> 2
		}
	}
}

@Parcelize
data class JourneyParams(val timeReference: TimeReference, val date: LocalDate?, val time: LocalTime?, val wheelchairAccessible: Boolean, val bicycle: Boolean) : Parcelable {
	fun getSafeDate(): LocalDate {
		return date ?: Instant.now().atZone(ZoneId.systemDefault()).toLocalDate()
	}

	fun getSafeTime(): LocalTime {
		return time ?: Instant.now().atZone(ZoneId.systemDefault()).toLocalTime()
	}
}