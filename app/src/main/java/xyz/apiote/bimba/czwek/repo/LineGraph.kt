// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import xyz.apiote.bimba.czwek.api.LineGraph

@Parcelize
data class LineGraph(
	val stops: List<StopStub>,
	val nextNodes: Map<Long, List<Long>>,
) : Parcelable {
	constructor(lineGraph: LineGraph) : this(
		lineGraph.stops.map { StopStub(it) },
		lineGraph.nextNodes
	)
}