// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.dashboard.ui.home

import android.os.Bundle
import android.os.CountDownTimer
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.search.SearchView.TransitionState
import com.google.android.material.snackbar.Snackbar
import xyz.apiote.bimba.czwek.R
import xyz.apiote.bimba.czwek.dashboard.MainActivity
import xyz.apiote.bimba.czwek.databinding.FragmentHomeBinding
import xyz.apiote.bimba.czwek.dpToPixelI
import xyz.apiote.bimba.czwek.repo.Favourite
import xyz.apiote.bimba.czwek.search.BimbaResultsAdapter
import xyz.apiote.bimba.czwek.units.Second

class HomeFragment : Fragment() {
	private var _binding: FragmentHomeBinding? = null
	private val binding get() = _binding!!

	private lateinit var adapter: BimbaResultsAdapter
	private lateinit var favouritesAdapter: BimbaFavouritesAdapter
	private lateinit var viewModel: HomeViewModel

	private val countdown =
		object : CountDownTimer(Second(30).milliseconds(), Second(10).milliseconds()) {
			override fun onTick(millisUntilFinished: Long) {
			}

			override fun onFinish() {
				refreshDepartures()
			}
		}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		_binding = FragmentHomeBinding.inflate(inflater, container, false)

		viewModel =
			ViewModelProvider(this)[HomeViewModel::class.java]
		viewModel.queryables.observe(viewLifecycleOwner) {
			adapter.feedsSettings = viewModel.feedsSettings
			adapter.feeds = viewModel.feeds
			adapter.update(it, null, false)
		}
		viewModel.favourites.observe(viewLifecycleOwner) {
			favouritesAdapter.updateFavourites(it)
			refreshDepartures()
		}
		viewModel.departures.observe(viewLifecycleOwner) {
			favouritesAdapter.updateDepartures(it)
		}

		countdown.start()

		binding.searchView.setupWithSearchBar(binding.searchBar)

		val onBackPressedCallback = object :
			OnBackPressedCallback(binding.searchView.currentTransitionState == TransitionState.SHOWN) {
			override fun handleOnBackPressed() {
				binding.searchBar.collapse(binding.searchView)
			}
		}
		activity?.onBackPressedDispatcher?.addCallback(onBackPressedCallback)
		binding.searchView.addTransitionListener { _, _, newState ->
			onBackPressedCallback.isEnabled = when (newState) {
				TransitionState.SHOWN -> true
				TransitionState.HIDDEN -> false
				else -> false
			}
			if (newState === TransitionState.HIDING) {
				adapter.update(listOf(), null, false)
			}
		}

		binding.searchBar.setNavigationOnClickListener {
			(context as MainActivity).onNavigationClicked()
		}
		binding.suggestionsRecycler.layoutManager = LinearLayoutManager(activity)
		adapter = BimbaResultsAdapter(layoutInflater, activity, listOf(), null, null, false)
		binding.suggestionsRecycler.adapter = adapter

		binding.searchView.editText.addTextChangedListener(
			viewModel.SearchBarWatcher(requireContext())
		)
		binding.searchView.editText.setOnKeyListener { v, keyCode, event ->
			when (keyCode) {
				KeyEvent.KEYCODE_ENTER -> {
					if (event.action == KeyEvent.ACTION_UP) {
						(context as MainActivity).onSearchClicked((v as TextView).text)
						true
					} else {
						false
					}
				}

				else -> false
			}
		}

		binding.floatingActionButton.setOnClickListener {
			(context as MainActivity).onGpsClicked(this)
		}

		ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, windowInsets ->
			val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
			binding.floatingActionButton.updateLayoutParams<ViewGroup.MarginLayoutParams> {
				rightMargin = insets.right + dpToPixelI(16f)
			}
			binding.searchBar.updateLayoutParams<ViewGroup.MarginLayoutParams> {
				rightMargin = insets.right + dpToPixelI(8f)
				leftMargin = insets.left + dpToPixelI(8f)
				topMargin = insets.top + dpToPixelI(8f)
			}
			binding.suggestionsRecycler.updatePadding(left = insets.left, right = insets.right)
			windowInsets.displayCutout?.safeInsetLeft?.let {
				binding.suggestionsRecycler.updateLayoutParams<ViewGroup.MarginLayoutParams> {
					leftMargin = it
				}
			}
			binding.favourites.updatePadding(left = insets.left, right = insets.right)
			binding.favourites.updateLayoutParams<ViewGroup.MarginLayoutParams> {
				windowInsets.displayCutout?.safeInsetLeft?.let {
					leftMargin = it + dpToPixelI(16f)
				}
				windowInsets.displayCutout?.safeInsetRight?.let {
					rightMargin = it + dpToPixelI(16f)
				}
			}
			windowInsets
		}

		binding.favourites.layoutManager = LinearLayoutManager(context)
		favouritesAdapter = BimbaFavouritesAdapter(listOf(), mapOf(), layoutInflater, requireContext())
		binding.favourites.adapter = favouritesAdapter

		viewModel.getFavourites(requireContext())

		val ith = ItemTouchHelper(object : ItemTouchHelper.Callback() {
			var newFavourites = emptyList<Favourite>()
			override fun onMove(
				recyclerView: RecyclerView,
				viewHolder: RecyclerView.ViewHolder,
				target: RecyclerView.ViewHolder
			): Boolean {
				newFavourites =
					favouritesAdapter.swap(viewHolder.absoluteAdapterPosition, target.absoluteAdapterPosition)
				return true
			}

			override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
				val (newFavourites, removedFavourite) = favouritesAdapter.delete(viewHolder.absoluteAdapterPosition)
				this.newFavourites = newFavourites
				Snackbar.make(binding.fragmentRoot, R.string.favourite_deleted, Snackbar.LENGTH_LONG)
					.setAction(R.string.undo) {
						this.newFavourites = favouritesAdapter.insert(removedFavourite)
						viewModel.saveFavourites(this.newFavourites, requireContext())
					}
					.show()
			}

			override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
				super.clearView(recyclerView, viewHolder)

				viewModel.saveFavourites(newFavourites, requireContext())
			}

			override fun getMovementFlags(
				recyclerView: RecyclerView,
				viewHolder: RecyclerView.ViewHolder
			): Int {
				return makeFlag(
					ItemTouchHelper.ACTION_STATE_DRAG,
					ItemTouchHelper.DOWN or ItemTouchHelper.UP
				) or makeFlag(
					ItemTouchHelper.ACTION_STATE_SWIPE,
					ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
				)
			}
		})
		ith.attachToRecyclerView(binding.favourites)


		return binding.root
	}

	fun refreshDepartures() {
		viewModel.getDepartures(requireContext())
		countdown.start()
	}

	override fun onResume() {
		super.onResume()
		viewModel.getFavourites(requireContext())
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
		countdown.cancel()
	}
}
