// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.appcompat.content.res.AppCompatResources
import xyz.apiote.bimba.czwek.R

class ErrorLocatable(val stringResource: Int) : Locatable {
	override fun icon(context: Context, scale: Float): Drawable {
		return AppCompatResources.getDrawable(context, R.drawable.error_other)!!
	}

	override fun location(): Position {
		return Position(0.0, 0.0)
	}

	override fun id(): String {
		return "ERROR"
	}
}