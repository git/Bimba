// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.content.Context
import android.graphics.drawable.Drawable
import xyz.apiote.bimba.czwek.api.Server
import java.time.LocalDate

interface Queryable {
	fun location(): Position?
}
interface Locatable {
	fun icon(context: Context, scale: Float = 1f): Drawable
	fun location(): Position
	fun id(): String
}

interface Repository {
	suspend fun getFavourite(stopCode: String): Favourite?
	suspend fun getFavourites(feedIDs: Set<String> = emptySet()): List<Favourite>

	suspend fun saveFavourite(favourite: Favourite)
	suspend fun saveFavourites(favourites: Set<Favourite>)

	suspend fun getFeeds(
		context: Context,
		server: Server = Server.get(context)
	): Map<String, FeedInfo>?

	suspend fun getDepartures(
		feedID: String,
		stop: String,
		date: LocalDate?,
		context: Context,
		limit: Int?,
		exact: Boolean
	): StopEvents?

	suspend fun getLocatablesIn(
		context: Context,
		bl: Position,
		tr: Position,
	): List<Locatable>?

	suspend fun getLine(
		context: Context,
		feedID: String,
		lineName: String,
		lineID: String,
	): Line?

	suspend fun queryQueryables(
		query: String,
		context: Context,
		feedID: String? = null
	): List<Queryable>?

	suspend fun locateQueryables(
		position: Position,
		context: Context
	): List<Queryable>?
}