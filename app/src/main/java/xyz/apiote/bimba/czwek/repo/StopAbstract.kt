// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.content.Context
import android.content.res.Configuration
import android.content.res.TypedArray
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.ColorUtils
import androidx.core.graphics.drawable.toBitmap
import xyz.apiote.bimba.czwek.R
import xyz.apiote.bimba.czwek.dpToPixelI
import java.util.zip.Adler32

interface StopAbstract{
	fun icon(context: Context, nodeName: String, scale: Float): Drawable {
		val md = Adler32().let {
			it.update(nodeName.toByteArray())
			it.value
		}
		val h = md % 359f
		val s = 1.0f
		val a: TypedArray = context.theme.obtainStyledAttributes(
			R.style.Theme_Bimba, intArrayOf(R.attr.randomColourLightness)
		)
		val l = a.getFloat(0, when (context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
			Configuration.UI_MODE_NIGHT_YES -> 1f
			Configuration.UI_MODE_NIGHT_NO -> 0f
			Configuration.UI_MODE_NIGHT_UNDEFINED -> 0f
			else -> 0f
		})
		a.recycle()
		val bg = AppCompatResources.getDrawable(context, R.drawable.stop)!!.mutate().apply {
			setTint(ColorUtils.HSLToColor(floatArrayOf(h, s, l)))
		}
		return BitmapDrawable(
			context.resources,
			bg.toBitmap(dpToPixelI(24f / scale), dpToPixelI(24f / scale), Bitmap.Config.ARGB_8888)
		)
	}
}