// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import xyz.apiote.bimba.czwek.api.transitous.model.Place
import xyz.apiote.bimba.czwek.units.DistanceUnit
import xyz.apiote.bimba.czwek.units.TimeUnit
import java.time.ZonedDateTime

data class Journey(val startTime: ZonedDateTime, val endTime: ZonedDateTime, val legs: List<Leg>)

data class Leg(
	val start: Event,
	val end: Event,
	val origin: xyz.apiote.bimba.czwek.repo.Place,
	val destination: xyz.apiote.bimba.czwek.repo.Place,
	val agencyName: String?,
	val distance: DistanceUnit?,
	val duration: TimeUnit,
	val intermediateStops: List<xyz.apiote.bimba.czwek.repo.Place>,
	val shape: List<Position>,
	/* TODO
	val rental: Rental?,
	val steps: List<StepInstruction>?,*/
)

@Parcelize
class Place(val stop: Stop, val latitude: Double, val longitude: Double) : Parcelable {
	constructor(place: Place) : this(
		stop = Stop(place),
		latitude = place.lat.toDouble(),
		longitude = place.lon.toDouble()
	)

	constructor(latitude: Double, longitude: Double) : this(
		Stop(
			"", "",
			"", "", null, Position(latitude, longitude), emptyList<ChangeOption>(), null
		), latitude, longitude
	)

	constructor(stop: Stop) : this(stop, stop.position.latitude, stop.position.longitude)

	fun position(): Position {
		return Position(latitude, longitude)
	}

	fun planString(): String = if (stop.code == "") {
		"${latitude},${longitude},0"
	} else {
		stop.code
	}

	fun shortString(): String = if (stop.name == "") {
		"%.2f, %.2f".format(
			latitude,
			longitude
		)
	} else {
		stop.name
	}
}
