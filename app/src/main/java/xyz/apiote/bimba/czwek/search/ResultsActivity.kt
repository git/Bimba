// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.search

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.openlocationcode.OpenLocationCode
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.Runnable
import kotlinx.coroutines.launch
import org.openapitools.client.infrastructure.ServerException
import xyz.apiote.bimba.czwek.R
import xyz.apiote.bimba.czwek.api.Error
import xyz.apiote.bimba.czwek.dashboard.MainActivity
import xyz.apiote.bimba.czwek.databinding.ActivityResultsBinding
import xyz.apiote.bimba.czwek.repo.OfflineRepository
import xyz.apiote.bimba.czwek.repo.OnlineRepository
import xyz.apiote.bimba.czwek.repo.Place
import xyz.apiote.bimba.czwek.repo.Position
import xyz.apiote.bimba.czwek.repo.Queryable
import xyz.apiote.bimba.czwek.repo.Stop
import xyz.apiote.bimba.czwek.repo.TrafficResponseException
import xyz.apiote.bimba.czwek.search.Query.Mode
import xyz.apiote.bimba.czwek.settings.feeds.FeedsSettings

class ResultsActivity : AppCompatActivity(), LocationListener, SensorEventListener {
	companion object {
		const val QUERY_KEY = "query"
		const val RETURN_KEY = "ret"
		const val FEED_KEY = "feed"
		fun getIntent(
			context: Context,
			query: Query,
			ret: Boolean = false,
			feedID: String? = null
		) =
			Intent(context, ResultsActivity::class.java).apply {
				putExtra(QUERY_KEY, query)
				putExtra(RETURN_KEY, ret)
				putExtra(FEED_KEY, feedID)
			}
	}

	private var _binding: ActivityResultsBinding? = null
	private val binding get() = _binding!!

	private lateinit var adapter: BimbaResultsAdapter

	private val handler = Handler(Looper.getMainLooper())
	private var runnable = Runnable {}
	private var gravity: FloatArray? = null
	private var geomagnetic: FloatArray? = null
	private var shortOLC: OpenLocationCode? = null
	private lateinit var query: Query

	override fun onCreate(savedInstanceState: Bundle?) {
		enableEdgeToEdge()
		super.onCreate(savedInstanceState)
		_binding = ActivityResultsBinding.inflate(layoutInflater)
		setContentView(binding.root)

		ViewCompat.setOnApplyWindowInsetsListener(binding.resultsRecycler) { v, windowInsets ->

			windowInsets.displayCutout?.safeInsetLeft?.let {
				v.updateLayoutParams<MarginLayoutParams> {
					leftMargin = it
				}
			}
			val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
			v.updatePadding(right = insets.right, left = insets.left)
			windowInsets
		}
		ViewCompat.setOnApplyWindowInsetsListener(binding.topAppBar) { v, windowInsets ->
			val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
			v.updatePadding(right = insets.right, left = insets.left)
			windowInsets
		}

		binding.resultsRecycler.layoutManager = LinearLayoutManager(this)
		adapter =
			BimbaResultsAdapter(layoutInflater, this, listOf(), null, null, false, getReturnResults())
		binding.resultsRecycler.adapter = adapter

		query = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
			intent.getParcelableExtra(QUERY_KEY, Query::class.java)!!
		} else {
			@Suppress("DEPRECATION")
			intent.getParcelableExtra(QUERY_KEY)!!
		}

		useQuery()
	}

	private fun useQuery() {
		when (query.mode) {
			Mode.LOCATION -> {
				binding.topAppBar.title = getString(R.string.stops_nearby)
				locate()
			}

			Mode.LOCATION_PLUS_CODE -> {
				binding.topAppBar.title = getString(R.string.stops_near_code, query.raw)
				shortOLC = OpenLocationCode(query.raw)
				locate()
			}

			Mode.UNKNOWN -> {
				try {
					query.parse(this)
					if (query.mode != Mode.UNKNOWN) {
						useQuery()
					} else {
						showError(Error(0, R.string.error_unknown, R.drawable.error_other))
					}
				} catch (_: GeocodingException) {
					showError(Error(0, R.string.error_geocoding, R.drawable.geocoding))
				}
			}

			Mode.POSITION -> {
				binding.topAppBar.title = getString(R.string.stops_near_code, query.raw)
				getQueryablesByLocation(Location(null).apply {
					latitude = query.position!!.latitude
					longitude = query.position!!.longitude
				}, this)
			}

			Mode.NAME -> {
				binding.topAppBar.title = getString(R.string.results_for, query.raw)
				getQueryablesByQuery(query.raw, this)
			}
		}
	}

	private fun getReturnResults(): Boolean = intent.extras?.getBoolean(RETURN_KEY) == true

	private fun locate() {
		val sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
		val accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
		val magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
		sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL)
		sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_NORMAL)

		try {
			val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
			locationManager.requestLocationUpdates(
				LocationManager.GPS_PROVIDER, 1000 * 60 * 10, 100f, this
			)
			handler.removeCallbacks(runnable)
			runnable = Runnable {
				showError(Error(0, R.string.error_gps, R.drawable.error_gps))
			}
			handler.postDelayed(runnable, 60 * 1000)
			locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
				?.let { onLocationChanged(it) }
		} catch (_: SecurityException) {
			Log.wtf(
				"locate",
				"this shouldn’t happen because we don’t start this activity without location permission"
			)
		}
	}

	override fun onLocationChanged(location: Location) {
		handler.removeCallbacks(runnable)
		val area = shortOLC?.recover(location.latitude, location.longitude)?.decode()
		if (area != null) {
			getQueryablesByLocation(Location(null).apply {
				latitude = area.centerLatitude
				longitude = area.centerLongitude
			}, this, false)
		} else {
			getQueryablesByLocation(location, this, true)
		}
	}

	override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
	override fun onSensorChanged(event: SensorEvent?) {
		if (event?.sensor?.type == Sensor.TYPE_ACCELEROMETER) gravity = event.values
		if (event?.sensor?.type == Sensor.TYPE_MAGNETIC_FIELD) geomagnetic = event.values
		if (gravity != null && geomagnetic != null) {
			val r = FloatArray(9)
			val success = SensorManager.getRotationMatrix(r, FloatArray(9), gravity, geomagnetic)
			if (success) {
				val orientation = FloatArray(3)
				SensorManager.getOrientation(r, orientation)
				adapter.update((orientation[0] * 180 / Math.PI).toFloat())
			}
		}
	}

	override fun onResume() {
		super.onResume()
		if (query.mode == Mode.LOCATION) {
			locate()
		}
	}

	override fun onPause() {
		super.onPause()
		val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
		locationManager.removeUpdates(this)
		handler.removeCallbacks(runnable)
		val sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
		sensorManager.unregisterListener(this)
	}

	override fun onDestroy() {
		super.onDestroy()
		val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
		locationManager.removeUpdates(this)
		handler.removeCallbacks(runnable)
	}

	private suspend fun getFeeds() {
		if (adapter.feeds.isNullOrEmpty()) {
			val repository = OfflineRepository(this)
			adapter.feeds = repository.getFeeds(this)
			repository.close()
		}
		if (adapter.feedsSettings == null) {
			adapter.feedsSettings = FeedsSettings.load(this)
		}
	}

	private fun getQueryablesByQuery(query: String, context: Context) {
		MainScope().launch {
			try {
				val repository = OnlineRepository()
				val result = repository.queryQueryables(query, context, intent.getStringExtra(FEED_KEY))
				getFeeds()
				updateItems(result, null, false)
			} catch (e: TrafficResponseException) {
				Log.w("Suggestion", "$e")
				showError(e.error)
			} catch (e: ServerException) {
				Log.w("Suggestion", "Transitous returned: ${e.statusCode}, ${e.message}")
				showError(Error.fromTransitous(e))
			}
		}
	}

	private fun getQueryablesByLocation(
		position: Location,
		context: Context,
		showArrow: Boolean = false
	) {
		MainScope().launch {
			try {
				val repository = OnlineRepository()
				val result =
					repository.locateQueryables(Position(position.latitude, position.longitude), context)
				getFeeds()
				updateItems(result, position, showArrow)
			} catch (e: TrafficResponseException) {
				Log.w("Suggestion", "$e")
				showError(e.error)
			} catch (e: ServerException) {
				Log.w("Suggestion", "Transitous returned: ${e.statusCode}, ${e.message}")
				showError(Error.fromTransitous(e))
			}
		}
	}

	private fun showError(error: Error) {
		binding.resultsProgress.visibility = View.GONE
		binding.resultsRecycler.visibility = View.GONE
		binding.errorImage.visibility = View.VISIBLE
		binding.errorText.visibility = View.VISIBLE

		binding.errorText.text = getString(error.stringResource)
		binding.errorImage.setImageDrawable(AppCompatResources.getDrawable(this, error.imageResource))
	}

	private fun updateItems(queryables: List<Queryable>?, position: Location?, showArrow: Boolean) {
		binding.resultsProgress.visibility = View.GONE
		adapter.update(queryables, position, showArrow)
		if (queryables.isNullOrEmpty()) {
			binding.errorImage.visibility = View.VISIBLE
			binding.errorText.visibility = View.VISIBLE
			binding.resultsRecycler.visibility = View.GONE

			binding.errorText.text = getString(R.string.error_404)
			binding.errorImage.setImageDrawable(
				AppCompatResources.getDrawable(
					this,
					R.drawable.error_search
				)
			)
		} else {
			if (queryables.size == 1 && query.mode == Query.Mode.NAME && !getReturnResults()) {
				adapter.click(0)
			}
			binding.resultsOverlay.visibility = View.GONE
			binding.errorImage.visibility = View.GONE
			binding.errorText.visibility = View.GONE
			binding.resultsRecycler.visibility = View.VISIBLE
		}
	}

	fun returnResult(stop: Stop) {
		setResult(RESULT_OK, Intent(this, MainActivity::class.java).apply {
			putExtra("PLACE", Place(stop))
		})
		finish()
	}
}
