// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import xyz.apiote.bimba.czwek.api.LineTypeV1
import xyz.apiote.bimba.czwek.api.LineTypeV2
import xyz.apiote.bimba.czwek.api.LineTypeV3
import xyz.apiote.bimba.czwek.api.transitous.model.Mode

enum class LineType {
	UNKNOWN, TRAM, BUS, TROLLEYBUS, METRO, RAIL, FERRY, CABLE_TRAM, CABLE_CAR, FUNICULAR, MONORAIL, PLANE, WALK;

	fun isActive(): Boolean {
		return this == WALK
	}

	fun isPasive(): Boolean {
		return !isActive()
	}

	companion object {
		fun of(t: LineTypeV1): LineType {
			return when (t) {
				LineTypeV1.UNKNOWN -> valueOf("UNKNOWN")
				LineTypeV1.TRAM -> valueOf("TRAM")
				LineTypeV1.BUS -> valueOf("BUS")
			}
		}
		fun of(t: LineTypeV2): LineType {
			return when (t) {
				LineTypeV2.UNKNOWN -> valueOf("UNKNOWN")
				LineTypeV2.TRAM -> valueOf("TRAM")
				LineTypeV2.BUS -> valueOf("BUS")
				LineTypeV2.TROLLEYBUS -> valueOf("TROLLEYBUS")
			}
		}
		fun of(t: LineTypeV3): LineType {
			return when (t) {
				LineTypeV3.UNKNOWN -> valueOf("UNKNOWN")
				LineTypeV3.TRAM -> valueOf("TRAM")
				LineTypeV3.BUS -> valueOf("BUS")
				LineTypeV3.TROLLEYBUS -> valueOf("TROLLEYBUS")
				LineTypeV3.METRO -> valueOf("METRO")
				LineTypeV3.RAIL -> valueOf("RAIL")
				LineTypeV3.FERRY -> valueOf("FERRY")
				LineTypeV3.CABLE_TRAM -> valueOf("CABLE_TRAM")
				LineTypeV3.CABLE_CAR -> valueOf("CABLE_CAR")
				LineTypeV3.FUNICULAR -> valueOf("FUNICULAR")
				LineTypeV3.MONORAIL -> valueOf("MONORAIL")
			}
		}

		fun fromTransitous2(mode: Mode): LineType {
			return when(mode) {
				Mode.AIRPLANE -> PLANE
				Mode.WALK -> WALK
				Mode.BIKE -> UNKNOWN
				Mode.CAR -> UNKNOWN
				Mode.RENTAL -> UNKNOWN
				Mode.TRANSIT -> UNKNOWN
				Mode.TRAM -> TRAM
				Mode.SUBWAY -> METRO
				Mode.FERRY -> FERRY
				Mode.METRO -> METRO
				Mode.BUS -> BUS
				Mode.COACH -> BUS
				Mode.RAIL -> RAIL
				Mode.HIGHSPEED_RAIL -> RAIL
				Mode.LONG_DISTANCE -> RAIL
				Mode.NIGHT_RAIL -> RAIL
				Mode.REGIONAL_FAST_RAIL -> RAIL
				Mode.REGIONAL_RAIL -> RAIL
				Mode.OTHER -> UNKNOWN
				Mode.CAR_PARKING -> UNKNOWN
			}
		}
	}
}