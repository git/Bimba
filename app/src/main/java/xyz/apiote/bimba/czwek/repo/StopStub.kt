// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import xyz.apiote.bimba.czwek.api.StopStub

@Parcelize
data class StopStub(val name: String, val nodeName: String, val code: String, val zone: String, val onDemand: Boolean) :
	Parcelable, StopAbstract {
	constructor(stopStub: StopStub) : this(
		stopStub.name,
		stopStub.nodeName,
		stopStub.code,
		stopStub.zone,
		stopStub.onDemand
	)
	fun icon(context: Context, scale: Float = 1f): Drawable {
		return super.icon(context, nodeName, scale)
	}
}