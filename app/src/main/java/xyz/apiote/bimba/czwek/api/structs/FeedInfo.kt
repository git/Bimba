// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api.structs

import xyz.apiote.fruchtfleisch.Reader
import java.io.InputStream
import java.time.LocalDate
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

data class FeedInfoV2(
	val name: String,
	val id: String,
	val attribution: String,
	val description: String,
	val lastUpdate: LocalDate,
	val qrHost: String,
	val qrIn: QrLocationV1,
	val qrSelector: String,
	val validSince: LocalDate,
	val validTill: LocalDate
) {
	companion object {
		fun unmarshal(stream: InputStream): FeedInfoV2 {
			val reader = Reader(stream)
			return FeedInfoV2(
				reader.readString(),
				reader.readString(),
				reader.readString(),
				reader.readString(),
				LocalDate.parse(reader.readString(), DateTimeFormatter.ISO_LOCAL_DATE),
				reader.readString(),
				QrLocationV1.of(reader.readUInt().toULong().toUInt()),
				reader.readString(),
				LocalDate.parse(reader.readString(), DateTimeFormatter.BASIC_ISO_DATE),
				LocalDate.parse(reader.readString(), DateTimeFormatter.BASIC_ISO_DATE)
			)
		}
	}
}

data class FeedInfoV1(
	val name: String,
	val id: String,
	val attribution: String,
	val description: String,
	val lastUpdate: ZonedDateTime
) {
	companion object {
		fun unmarshal(stream: InputStream): FeedInfoV1 {
			val reader = Reader(stream)
			return FeedInfoV1(
				reader.readString(),
				reader.readString(),
				reader.readString(),
				reader.readString(),
				ZonedDateTime.parse(reader.readString(), DateTimeFormatter.ISO_DATE_TIME)
			)
		}
	}
}
