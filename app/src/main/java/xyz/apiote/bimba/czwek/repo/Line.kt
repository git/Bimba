// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.repo

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import xyz.apiote.bimba.czwek.api.LineV1
import xyz.apiote.bimba.czwek.api.LineV2
import xyz.apiote.bimba.czwek.api.LineV3

data class Line(
	val id: String,
	val name: String,
	val colour: Colour,
	val type: LineType,
	val feedID: String,
	val headsigns: List<List<String>>,
	val graphs: List<LineGraph>,
) : Queryable, LineAbstract {

	constructor(line: LineV1) : this(
		line.name,
		line.name,
		Colour(line.colour),
		LineType.of(line.type),
		line.feedID,
		line.headsigns,
		line.graphs.map{LineGraph(it)}
	)
	constructor(line: LineV2) : this(
		line.name,
		line.name,
		Colour(line.colour),
		LineType.of(line.type),
		line.feedID,
		line.headsigns,
		line.graphs.map{LineGraph(it)}
	)

	constructor(line: LineV3) : this(
		line.id,
		line.name,
		Colour(line.colour),
		LineType.of(line.type),
		line.feedID,
		line.headsigns,
		line.graphs.map{LineGraph(it)}
	)

	fun icon(context: Context, scale: Float = 1f): Drawable {
		return BitmapDrawable(context.resources, super.icon(context, type, colour, scale))
	}

	override fun location(): Position? {
		return null
	}
}