// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import xyz.apiote.bimba.czwek.databinding.ActivityAboutBinding
import kotlin.math.max


class AboutActivity : AppCompatActivity() {
	private var _binding: ActivityAboutBinding? = null
	private val binding get() = _binding!!
	override fun onCreate(savedInstanceState: Bundle?) {
		enableEdgeToEdge()
		super.onCreate(savedInstanceState)
		_binding = ActivityAboutBinding.inflate(layoutInflater)
		setContentView(binding.root)

		ViewCompat.setOnApplyWindowInsetsListener(binding.root) { v, windowInsets ->
			val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
			val l = max(windowInsets.displayCutout?.safeInsetLeft ?: 0, insets.left)
			val r = max(windowInsets.displayCutout?.safeInsetRight ?: 0, insets.right)
			val t = max(windowInsets.displayCutout?.safeInsetTop ?: 0, insets.top)
			v.updatePadding(left = l, right = r, top = t)
			windowInsets
		}

		binding.mastodon.setOnClickListener {
			startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://floss.social/@bimba")))
		}
		binding.website.setOnClickListener {
			startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://bimba.app")))
		}
		binding.code.setOnClickListener {
			startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://git.apiote.xyz/Bimba.git")))
		}
		binding.email.setOnClickListener {
			val intent = Intent(Intent.ACTION_SENDTO).apply {
				setData(Uri.parse("mailto:questions@bimba.app"))
			}
			try {
				startActivity(intent)
			} catch (_: ActivityNotFoundException) {
				Toast.makeText(this, getString(R.string.no_email_app), Toast.LENGTH_SHORT).show()
			}
		}
		binding.translate.setOnClickListener {
			startActivity(
				Intent(
					Intent.ACTION_VIEW,
					Uri.parse("https://hosted.weblate.org/projects/bimba/")
				)
			)
		}
		binding.matrix.setOnClickListener {
			startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://matrix.to/#/#marblearch:apiote.xyz")))
		}
	}
}
