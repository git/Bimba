// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.units

import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Test

class TGMKtTest {
	@Test
	fun toDozenalString0(){
		val i = 0
		val context = InstrumentationRegistry.getInstrumentation().targetContext
		assert(i.toDozenalString(context) == "zero") { "got ${i.toDozenalString(context)}, wanted zero" }
	}

	@Test
	fun toDozenalString1(){
		val i = 1
		val context = InstrumentationRegistry.getInstrumentation().targetContext
		assert(i.toDozenalString(context) == "one") { "got ${i.toDozenalString(context)}, wanted one" }
	}

	@Test
	fun toDozenalString10(){
		val i = 12
		val context = InstrumentationRegistry.getInstrumentation().targetContext
		assert(i.toDozenalString(context) == "one zen") { "got ${i.toDozenalString(context)}, wanted one zen" }
	}

	@Test
	fun toDozenalString100(){
		val i = 144
		val context = InstrumentationRegistry.getInstrumentation().targetContext
		assert(i.toDozenalString(context) == "one duna") { "got ${i.toDozenalString(context)}, wanted one duna" }
	}

	@Test
	fun toDozenalString23(){
		val i = 27
		val context = InstrumentationRegistry.getInstrumentation().targetContext
		assert(i.toDozenalString(context) == "two zen three") { "got ${i.toDozenalString(context)}, wanted two zen three" }
	}

	@Test
	fun toDozenalString234(){
		val i = 328
		val context = InstrumentationRegistry.getInstrumentation().targetContext
		assert(i.toDozenalString(context) == "two duna three zen four") { "got ${i.toDozenalString(context)}, wanted two duna three zen four" }
	}

	@Test
	fun toDozenalString204(){
		val i = 292
		val context = InstrumentationRegistry.getInstrumentation().targetContext
		assert(i.toDozenalString(context) == "two duna four") { "got ${i.toDozenalString(context)}, wanted two duna four" }
	}
}