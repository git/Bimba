// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api.responses

import xyz.apiote.bimba.czwek.api.AlertV1
import xyz.apiote.bimba.czwek.api.DepartureV4
import xyz.apiote.bimba.czwek.api.LineV3
import xyz.apiote.bimba.czwek.api.ColourV1
import xyz.apiote.bimba.czwek.api.LineTypeV3
import xyz.apiote.bimba.czwek.api.LocatableV4
import xyz.apiote.bimba.czwek.api.StopV2
import xyz.apiote.bimba.czwek.api.PositionV1
import xyz.apiote.bimba.czwek.api.QueryableV5
import xyz.apiote.bimba.czwek.api.structs.FeedInfoV2
import java.io.InputStream

data class DeparturesResponseDev(
	val alerts: List<AlertV1>,
	val departures: List<DepartureV4>,
	val stop: StopV2
) : DeparturesResponse {
	companion object {
		private fun unmarshal(stream: InputStream): DeparturesResponseDev {
			return DeparturesResponseDev(listOf(), listOf(), StopV2("","","","","", PositionV1(0.0, 0.0), listOf()))
		}
	}
}

data class FeedsResponseDev(
	val feeds: List<FeedInfoV2>
) : FeedsResponse {
	companion object {
		private fun unmarshal(stream: InputStream): FeedsResponseDev {
			return FeedsResponseDev(listOf())
		}
	}
}

data class LineResponseDev(
	val line: LineV3
) : LineResponse {
	companion object {
		private fun unmarshal(stream: InputStream): LineResponseDev {
			return LineResponseDev(LineV3("","",ColourV1(0u,0u,0u),LineTypeV3.UNKNOWN,"",listOf(),listOf()))
		}
	}
}

data class LocatablesResponseDev(val locatables: List<LocatableV4>) : LocatablesResponse {
	companion object {
		private fun unmarshal(stream: InputStream): LocatablesResponseDev {
			return LocatablesResponseDev(listOf())
		}
	}
}


data class QueryablesResponseDev(val queryables: List<QueryableV5>) : QueryablesResponse {
	companion object {
		private fun unmarshal(stream: InputStream): QueryablesResponseDev {
			return QueryablesResponseDev(listOf())
		}
	}
}
