// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.bimba.czwek.api.responses

import xyz.apiote.bimba.czwek.api.AlertV1
import xyz.apiote.bimba.czwek.api.DepartureV5
import xyz.apiote.bimba.czwek.api.LineV3
import xyz.apiote.bimba.czwek.api.LocatableV4
import xyz.apiote.bimba.czwek.api.QueryableV5
import xyz.apiote.bimba.czwek.api.StopV3
import xyz.apiote.bimba.czwek.api.UnknownResourceVersionException
import xyz.apiote.bimba.czwek.api.VehicleV3
import xyz.apiote.bimba.czwek.api.structs.FeedInfoV2
import xyz.apiote.fruchtfleisch.Reader
import java.io.InputStream

data class DeparturesResponseDev(
	val alerts: List<AlertV1>,
	val departures: List<DepartureV5>,
	val stop: StopV3
) : DeparturesResponse {
	companion object {
		fun unmarshal(stream: InputStream): DeparturesResponseDev {
			val alerts = mutableListOf<AlertV1>()
			val departures = mutableListOf<DepartureV5>()

			val reader = Reader(stream)
			val alertsNum = reader.readUInt().toULong()
			for (i in 0UL until alertsNum) {
				val alert = AlertV1.unmarshal(stream)
				alerts.add(alert)
			}
			val departuresNum = reader.readUInt().toULong()
			for (i in 0UL until departuresNum) {
				val departure = DepartureV5.unmarshal(stream)
				departures.add(departure)
			}

			return DeparturesResponseDev(alerts, departures, StopV3.unmarshal(stream))
		}
	}
}

data class FeedsResponseDev(
	val feeds: List<FeedInfoV2>
) : FeedsResponse {
	companion object {
		fun unmarshal(stream: InputStream): FeedsResponseDev {
			val feeds = mutableListOf<FeedInfoV2>()
			val reader = Reader(stream)
			val n = reader.readUInt().toULong()
			for (i in 0UL until n) {
				feeds.add(FeedInfoV2.unmarshal(stream))
			}
			return FeedsResponseDev(feeds)
		}
	}
}

data class LineResponseDev(
	val line: LineV3
) : LineResponse {
	companion object {
		fun unmarshal(stream: InputStream): LineResponseDev {
			return LineResponseDev(LineV3.unmarshal(stream))
		}
	}
}

data class LocatablesResponseDev(val locatables: List<LocatableV4>) : LocatablesResponse {
	companion object {
		fun unmarshal(stream: InputStream): LocatablesResponseDev {
			val locatables = mutableListOf<LocatableV4>()
			val reader = Reader(stream)
			val n = reader.readUInt().toULong()
			for (i in 0UL until n) {
				when (val r = reader.readUInt().toULong()) {
					0UL -> {
						locatables.add(StopV3.unmarshal(stream))
					}

					1UL -> {
						locatables.add(VehicleV3.unmarshal(stream))
					}

					else -> {
						throw UnknownResourceVersionException("Locatable/$r", 0u)
					}
				}
			}
			return LocatablesResponseDev(locatables)
		}
	}
}


data class QueryablesResponseDev(val queryables: List<QueryableV5>) : QueryablesResponse {
	companion object {
		fun unmarshal(stream: InputStream): QueryablesResponseDev {
			val queryables = mutableListOf<QueryableV5>()
			val reader = Reader(stream)
			val n = reader.readUInt().toULong()
			for (i in 0UL until n) {
				when (val r = reader.readUInt().toULong()) {
					0UL -> {
						queryables.add(StopV3.unmarshal(stream))
					}

					1UL -> {
						queryables.add(LineV3.unmarshal(stream))
					}

					else -> {
						throw UnknownResourceVersionException("Queryable/$r", 0u)
					}
				}
			}
			return QueryablesResponseDev(queryables)
		}
	}
}
