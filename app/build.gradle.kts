import com.android.build.gradle.internal.tasks.factory.dependsOn
import com.android.build.gradle.tasks.ExtractDeepLinksTask
import com.android.build.gradle.tasks.MergeResources
import de.undercouch.gradle.tasks.download.Download
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

plugins {
	id("com.android.application")
	kotlin("android")
	kotlin("plugin.parcelize")
	kotlin("plugin.serialization")
	id("org.openapi.generator")
	id("de.undercouch.download")
}

android {
	namespace = "xyz.apiote.bimba.czwek"
	// NOTE apksigner with `--alignment-preserved` https://gitlab.com/fdroid/fdroiddata/-/issues/3299#note_1989808414
	compileSdk = 35
	buildToolsVersion = "35.0.0"

	defaultConfig {
		applicationId = "xyz.apiote.bimba.czwek"
		minSdk = 21
		targetSdk = 35
		versionCode = 35
		versionName = "3.9.0-rc.1"

		testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
		resourceConfigurations += listOf("en", "de", "en-rGB", "en-rUS", "et", "fr", "it", "pl", "ru", "ta")
	}

	buildTypes {
		getByName("release") {
			isMinifyEnabled = false
			proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
		}
	}

	applicationVariants.configureEach {
		resValue("string", "versionName", versionName)
		resValue("string", "applicationId", applicationId)
	}

	compileOptions {
		sourceCompatibility = JavaVersion.VERSION_21
		targetCompatibility = JavaVersion.VERSION_21
		isCoreLibraryDesugaringEnabled = true
	}

	buildFeatures {
		viewBinding = true
	}
}

tasks.register<Download>("downloadFile") {
	src("https://bimba.app/transitous/openapi.yml")
	dest("${layout.buildDirectory.get()}/motis2.yml")
	overwrite(false)
}

tasks.register<Exec>("lintReuse") {
	commandLine("reuse", "lint")
}

afterEvaluate {
    tasks.named("preDebugBuild").dependsOn("lintReuse")
}

tasks.named("openApiGenerate").dependsOn("downloadFile")

openApiGenerate {
	generatorName.set("kotlin")
	inputSpec.set("${layout.buildDirectory.get()}/motis2.yml")
	outputDir.set("${layout.buildDirectory.get()}/generated")
	apiPackage.set("xyz.apiote.bimba.czwek.api.transitous.api")
	invokerPackage.set("xyz.apiote.bimba.czwek.api.transitous.invoker")
	modelPackage.set("xyz.apiote.bimba.czwek.api.transitous.model")
}

kotlin.sourceSets["main"].kotlin.srcDir("${layout.buildDirectory.get()}/generated/src/main/kotlin")

tasks.withType<KotlinCompile> {
	dependsOn("openApiGenerate")
}
tasks.withType<MergeResources> {
	dependsOn("openApiGenerate")
}
tasks.withType<ExtractDeepLinksTask> {
	dependsOn("openApiGenerate")
}

dependencies {
	implementation("androidx.core:core-ktx:1.15.0")
	implementation("androidx.appcompat:appcompat:1.7.0")
	implementation("com.google.android.material:material:1.12.0")
	implementation("androidx.constraintlayout:constraintlayout:2.2.0")
	implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.8.7")
	implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.8.7")
	implementation("androidx.navigation:navigation-fragment-ktx:2.8.6")
	implementation("androidx.navigation:navigation-ui-ktx:2.8.6")
	implementation("androidx.legacy:legacy-support-v4:1.0.0")
	implementation("androidx.core:core-splashscreen:1.0.1")
	implementation("com.google.openlocationcode:openlocationcode:1.0.4")
	implementation("org.osmdroid:osmdroid-android:6.1.20")
	implementation("org.yaml:snakeyaml:2.3")
	implementation("androidx.activity:activity-ktx:1.10.0")
	implementation("com.otaliastudios:zoomlayout:1.9.0")
	implementation("dev.bandb.graphview:graphview:0.8.1")
	implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.8.0")
	implementation("com.github.jershell:kbson:0.5.0")
	implementation("androidx.preference:preference-ktx:1.2.1")
	implementation("androidx.work:work-runtime-ktx:2.10.0")
	implementation("com.github.doyaaaaaken:kotlin-csv-jvm:1.10.0")
	implementation("commons-io:commons-io:2.18.0")
	implementation("com.google.guava:guava:33.4.0-android")
	implementation(project(":fruchtfleisch"))
	implementation("ch.acra:acra-http:5.12.0")
	implementation("ch.acra:acra-notification:5.12.0")
	implementation("com.squareup.okhttp3:okhttp:4.12.0")
	implementation("com.squareup.moshi:moshi-kotlin:1.15.2")
	implementation("androidx.activity:activity:1.10.0")

	coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:2.1.4")

	androidTestImplementation("androidx.test.ext:junit:1.2.1")
	androidTestImplementation("androidx.test.espresso:espresso-core:3.6.1")
}
