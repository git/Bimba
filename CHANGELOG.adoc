// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

= Changelog

All notable changes to this project will be documented in this file.

The format is based on https://keepachangelog.com/en/1.0.0/[Keep a Changelog], but uses AsciiDoc instead of Markdown,
and this project adheres to https://semver.org/spec/v2.0.0.html[Semantic Versioning].

== Unreleased

* Travel planning
* Offline timetable

== [3.9.0] – 2025-02-08

=== Added

* Travel planning based on Transitous

== [3.6.1] – 2024-09-04

=== Fixed

* phantom feed names in search results

=== Added

* stops and departures from Transitous

== [3.6.1] – 2024-09-04

=== Fixed

* add empty default cache credentials

== [3.6] – 2024-08-30

=== Added

* reporting crashes with ACRA
* use kotlin build scripts and cache
* use elizabeth dev responses to show inexact times
* use elizabeth dev responses to show coloured/italics lines in change options
* use elizabeth dev responses to grey-out or hide terminus arrivals

=== Fixed

* fixed updating conflicting rows for favourites and geonames on older Android versions

== [3.5] – 2024-07-24

=== Added

* geocoding short OLCs
* arrows directing to nearest stops
* unit systems

== [3.4] – 2024-07-03

=== Added

* favourites

== [3.3.2] – 2024-05-22

=== Changed

* Openstreetmap attribution
* Changed minimal map zoom level

== [3.3.1] – 2024-05-21

=== Fixed

* Limited map zoom leves so that markers scaling won’t crash

== [3.3] – 2024-05-16

=== Added

* Alerts
* Selecting date
* Filtering by time or line
* Automatically adding locales from build.gradle

=== Changed

* Cached localities have lower alpha
* Updated deprecated methods
* Updated dependencies

=== Fixed

* Landscape about screen
* Vehicle capabilities on map
* Lines directions other than 2
* Feed info is cached, not latest response

== [3.2] – 2024-03-13

=== Added

* about screen
* scroll to bottom loading more departures
* locality names in search results
* filtering over 12 items in localities list
* last update of departures if over a minute
* German translations
* REUSE compliancy
* offline cache of localities

=== Changed

* dependency updates, bug fixes, and some code refactor
* localities settings
* updating lists using DiffUtil
* stop icons shape and colour
* scaling stop icons
* TRAFFIC version clockworkOrange compatibility

== [3.1.1]

=== Fixed

* continue button in server selection

== [3.1]

=== Added

* monochrome launcher icon and predictive back gesture for Android 12
* Material You theme with system colors
* searching by Open Location Code
* Polish and Italian translations
* multiple feeds handling
* QR codes in Metropolis GZM
* searching by line (and picking stops on graphs)

=== Changed

* new search bar
* gliding instead of jumping in minimap
* dependency updates, bug fixes, and some code refactor

== [3.0] – 2023-04-11

=== Changed

* completely new architecture

== [2.2.2] – 2019-03-11

=== Changed

* drop HTML formatting in PEKA messages

== [2.2.1] – 2019-03-04

=== Changed

* white icons (low floor, tickets) in night mode

== [2.2.0] – 2019-02-26

=== Added

* showing low floor and ticket checkouts in VM departures

=== Changed

* departures empty state is semi-transparent

== [2.1] – 2019-02-04

=== Added

* showing empty search result
* loading in shed selection and stop screen
* VM messages

=== Changed

* search bar
* empty departures state
* ‘now’ departure is ‘in a moment’ if the vehicle is not on-stop
* sorting departures: on-stop at the top
* sorting search results by similarity

== [2.0] – 2018-09-21

=== Added

* official timetable from ZTM

=== Changed

* VM can be used without offline timetable
* offline timetable uses exact dates (instead of workdays/saturdays/holidays)
* VM is quicker and is more reliable (as it’s computed in the same way as offline departures)
* favourites rewritten from scratch
* app is movable to external storage
* new colours—grey and green—fitting new Poznań style

=== Fixed

* multiple bug fixes
