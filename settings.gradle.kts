// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
val cacheUser: String by settings
val cachePass: String by settings
buildCache {
    remote<HttpBuildCache> {
        url = uri("https://cranberry.apiote.xyz")
        isPush = true
        credentials {
            username = cacheUser
            password = cachePass
        }
        // on command line add -PcacheUser=user -PcachePassword=password or in ~/.gradle/gradle.properies
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.name = "Bimba"
include(":app")
include(":fruchtfleisch")
