// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.fruchtfleisch

data class IntVar(private val v: Long) {
	fun toLong() = v
}
data class UIntVar(private val v: ULong) {
	fun toULong() = v
}