// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.fruchtfleisch

import org.junit.jupiter.api.Test


@OptIn(ExperimentalUnsignedTypes::class)
class ReaderTest {
	@Test
	fun readUInt17() {
		val stream = byteArrayOf(0x11).inputStream()
		val reader = Reader(stream)
		assert(reader.readUInt().toULong().toInt() == 17)
	}
	@Test
	fun readUInt23() {
		val stream = byteArrayOf(0x17).inputStream()
		val reader = Reader(stream)
		assert(reader.readUInt().toULong().toInt() == 23)
	}
	@Test
	fun readUInt999() {
		val stream = ubyteArrayOf(0xe7u, 0x7u).toByteArray().inputStream()
		val reader = Reader(stream)
		assert(reader.readUInt().toULong().toInt() == 999)
	}

	@Test
	fun readInt() {
	}

	@Test
	fun readU8() {
	}

	@Test
	fun readU16() {
	}

	@Test
	fun readU32() {
	}

	@Test
	fun readU64() {
	}

	@Test
	fun readI8() {
	}

	@Test
	fun readI16() {
	}

	@Test
	fun readI32() {
	}

	@Test
	fun readI64() {
	}

	@Test
	fun readFloat32() {
	}

	@Test
	fun readFloat64() {
	}

	@Test
	fun readData() {
	}

	@Test
	fun readStringAscii() {
		val stream = byteArrayOf(0x24, 0x4d, 0x72, 0x2e, 0x20, 0x4a, 0x6f, 0x63, 0x6b, 0x2c, 0x20, 0x54, 0x56, 0x20, 0x71, 0x75, 0x69, 0x7a, 0x20, 0x50, 0x68, 0x44, 0x2c, 0x20, 0x62, 0x61, 0x67, 0x73, 0x20, 0x66, 0x65, 0x77, 0x20, 0x6c, 0x79, 0x6e, 0x78).inputStream()
		val reader = Reader(stream)
		assert(reader.readString() == "Mr. Jock, TV quiz PhD, bags few lynx")
	}

	@Test
	fun readStringUnicode() {
		val stream = ubyteArrayOf(0x34u, 0x53u, 0x74u, 0x72u, 0xc3u, 0xb3u, 0xc5u, 0xbcu, 0x20u, 0x70u, 0x63u, 0x68u, 0x6eu, 0xc4u, 0x85u, 0xc5u, 0x82u, 0x20u, 0x6bu, 0x6fu, 0xc5u, 0x9bu, 0xc4u, 0x87u, 0x20u, 0x77u, 0x20u, 0x71u, 0x75u, 0x69u, 0x7au, 0x20u, 0x67u, 0xc4u, 0x99u, 0x64u, 0xc5u, 0xbau, 0x62u, 0x20u, 0x76u, 0x65u, 0x6cu, 0x20u, 0x66u, 0x61u, 0x78u, 0x20u, 0x6du, 0x79u, 0x6au, 0xc5u, 0x84u).toByteArray().inputStream()
		val reader = Reader(stream)
		assert(reader.readString() == "Stróż pchnął kość w quiz gędźb vel fax myjń")
	}

	@Test
	fun readBoolean() {
	}
}