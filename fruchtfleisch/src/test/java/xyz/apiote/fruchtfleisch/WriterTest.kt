// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

package xyz.apiote.fruchtfleisch

import org.junit.jupiter.api.Test
import java.io.ByteArrayOutputStream

@OptIn(ExperimentalUnsignedTypes::class)
class WriterTest {

	@Test
	fun writeUInt17() {
		val stream = ByteArrayOutputStream()
		val writer = Writer(stream)
		writer.writeUInt(17u)
		val bytes = stream.toByteArray()
		assert(bytes.contentEquals(byteArrayOf(0x11)))
	}

	@Test
	fun writeUInt23() {
		val stream = ByteArrayOutputStream()
		val writer = Writer(stream)
		writer.writeUInt(23u)
		val bytes = stream.toByteArray()
		assert(bytes.contentEquals(byteArrayOf(0x17)))
	}

	@Test
	fun writeUInt999() {
		val stream = ByteArrayOutputStream()
		val writer = Writer(stream)
		writer.writeUInt(999u)
		val bytes = stream.toByteArray().toUByteArray()
		assert(bytes.contentEquals(ubyteArrayOf(0xe7u, 0x7u)))
	}

	@Test
	fun writeStringAscii() {
		val stream = ByteArrayOutputStream()
		val writer = Writer(stream)
		writer.writeString("Mr. Jock, TV quiz PhD, bags few lynx")
		val bytes = stream.toByteArray()
		assert(bytes.contentEquals(byteArrayOf(0x24, 0x4d, 0x72, 0x2e, 0x20, 0x4a, 0x6f, 0x63, 0x6b, 0x2c, 0x20, 0x54, 0x56, 0x20, 0x71, 0x75, 0x69, 0x7a, 0x20, 0x50, 0x68, 0x44, 0x2c, 0x20, 0x62, 0x61, 0x67, 0x73, 0x20, 0x66, 0x65, 0x77, 0x20, 0x6c, 0x79, 0x6e, 0x78)))
	}
}