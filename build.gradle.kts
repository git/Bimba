// SPDX-FileCopyrightText: Adam Evyčędo
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    id("com.android.application") version "8.8.0" apply false
    id("com.android.library") version "8.8.0" apply false
    id("org.openapi.generator") version "7.9.0" apply false
    id("de.undercouch.download") version "5.6.0" apply false
    kotlin("android") version "2.0.10" apply false
    kotlin("jvm") version "1.7.20" apply false
    kotlin("plugin.parcelize") version "1.8.20" apply false
    kotlin("plugin.serialization") version "1.9.22" apply false
}
